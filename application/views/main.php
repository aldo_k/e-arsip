<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-inverse navbar-submenu">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#module-submenu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Data Arsip</a>
		</div>

		<form class="navbar-form navbar-left width-half-full" method="get" action="<?php echo site_url('/home/search'); ?>">
			<div class="input-group width-full">
				<input type="text" name="katakunci" class="form-control" placeholder="nomor arsip/kata kunci uraian" /><span class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button></span>
				</div>
			</form>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="module-submenu">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" role="button" data-toggle="collapse" data-target="#advanced-search" 
						aria-expanded="false" aria-controls="advanced-search" 
						class="open-advanced-search"><i class="glyphicon glyphicon-search"></i> Pencarian Lanjut</a></li>
						<li><a href="<?php echo site_url('/home/dl').($_SERVER['QUERY_STRING']? '?'.$_SERVER['QUERY_STRING'] : '') ?>"><i class="glyphicon glyphicon-download"></i> Download Data</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

		<?php echo $this->session->flashdata('zz'); ?>
		<div class="panel panel-default panel-hidden collapse" id="advanced-search">
			<div class="panel-heading"><h3 class="panel-title">Pencarian Lanjut</h3></div>
			<div class="panel-body">
				<form action="<?php echo site_url('/home/search'); ?>" method="get" id="srcmain">
					<div class = "input-group">
						<span class = "input-group-addon">Uraian</span>
						<input id="uraian" name="uraian" class="form-control input-md" type="text" value="<?php echo $src['uraian'] ?>">
						<span class = "input-group-addon">No Surat/Arsip</span>
						<input id="noarsip" name="noarsip" class="form-control input-md" type="text" value="<?php echo $src['noarsip'] ?>">
						<span class = "input-group-addon">Pencipta arsip</span>
						<select class="form-control" name="pencipta" id="pencipta">
							<option value="all" >Semua</option>
							<?php
							if(isset($pencipta)) {
								foreach($pencipta as $p) {
									echo "<option value=\"".$p['id']."\" ".($src['pencipta']==$p['id']?"selected=selected":"").">"." - ".$p['nama_pencipta']."</option>";
								}
							}
							?>
						</select>
						<span class = "input-group-addon">Lokasi</span>
						<select class="form-control" name="lok" id="lok">
							<option value="all" >Semua</option>
							<?php
							if(isset($lok)) {
								foreach($lok as $p) {
									echo "<option value=\"".$p['id']."\" ".($src['lok']==$p['id']?"selected=selected":"").">"." - ".$p['nama_lokasi']."</option>";
								}
							}
							?>
						</select>
					</div>
					<br/>
					<div class = "input-group">
						<span class = "input-group-addon">Unit pengolah</span>
						<select class="form-control" name="peng" id="peng">
							<option value="all" >Semua</option>
							<?php
							if(isset($peng)) {
								foreach($peng as $p) {
									echo "<option value=\"".$p['id']."\" ".($src['peng']==$p['id']?"selected=selected":"").">"." - ".$p['nama_pengolah']."</option>";
								}
							}
							?>
						</select>
						<span class = "input-group-addon">Tanggal (yyyy-mm-dd)</span>
						<input id="tanggal" name="tanggal" class="form-control input-md" type="text" value="<?php echo $src['tanggal'] ?>">
						<span class = "input-group-addon">Ket</span>
						<select class="form-control" name="ket" id="ket">
							<option value="all" >Semua</option>
							<option value="asli" <?php echo ($src['ket']=='asli'?'selected=selected':''); ?> >Asli</option>
							<option value="copy" <?php echo ($src['ket']=='copy'?'selected=selected':''); ?> >Copy</option>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit" id="go"> Cari</button>
						</span>
					</div>
				</form>
			</div>
			<!-- ./panel body -->
		</div>
		<!-- ./panel -->

		<!-- Title -->
		<div class="well well-sm">
			<div class="row">
				<div class="col-xs-9">Ditemukan data sebanyak : <em class='small'>(<?php echo number_format($jml); ?>)</em> arsip</div>
				<div class="col-xs-3 text-right"></div>
			</div>
		</div>
		<!-- /.row -->
		<!-- Page Features -->
		<div class="row table-responsive" id="hslsrc">
			<table id="tblhslsrc" class="col-sm-12 table table-bordered table-hover">
				<thead>
					<tr>
						<th>No Arsip</th>
						<th>Tanggal</th>
						<th>Uraian</th>
						<th>Ket</th>
						<th>File</th>
						<th>Jumlah</th>
						<th width="125px">Tindakan</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($data as $a) {
						echo "<tr>";
						echo "<td>".$a['noarsip']."</td>";
						echo "<td>".$a['tanggal']."</td>";
						echo "<td>".$a['uraian']."</td>";
						echo "<td>".$a['ket']."</td>";
						if (!@$_SESSION['akses_modul']['arsip_download']=='on') {
							echo "<td><a href='#' class='btn-sm btn-danger' title='Anda Tidak Diizinkan Mendownload Arsip' ondblclick='alert(\"Anda Tidak Diizinkan Mendownload Arsip\")' disabled><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></a></td>";
						} else if($a['file']=="") {
							echo "<td><a href='#' class='btn-sm btn-danger' title='File Tidak Tersedia' ondblclick='alert(\"File Tidak Tersedia\")' disabled><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></a></td>";
						}else {
							echo "<td><a href='".base_url('index.php/arsip/download/'.$a['file'])."' target='_blank' class='btn-sm btn-success' title='Download File'><span class='glyphicon glyphicon-save' aria-hidden='true'></span></a></td>";
						}
						echo "<td>".$a['jumlah']."</td>";
						echo "<td>";
						echo "<a href='".site_url('home/view/'.$a['id'])."' class='btn-sm btn-info' title='Lihat Detail'><i class=\"glyphicon glyphicon-eye-open\"></i></a> ";
						if(@$_SESSION['akses_modul']['arsip_edit']=='on'){
							echo "<a href='".site_url('/arsip/perbarui/'.$a['id'])."' class='btn-sm btn-warning' title='Ubah Informasi'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
						}
						if(@$_SESSION['akses_modul']['arsip_hapus']=='on'){
							echo "<a class='deldata btn-sm btn-danger' id='".$a['id']."' href='#' data-toggle=\"modal\" data-target=\"#deldata\" title='Hapus'><i class=\"glyphicon glyphicon-trash\"></i></a>";
						}
						echo "</td>";
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
		<?php
		echo $pages;
		?>
		<!-- /.row -->
		<div class="modal fade" id="deldata">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Delete Data</h4>
					</div>
					<div class="modal-body">
						<form id="fdeldata" class="form-horizontal" role="form" method="post" action="<?php echo site_url("/arsip/hapus_arsip"); ?>">
							<h4 class="modal-title">Yakin ingin Hapus Data ini?</h4>
							<input type="hidden" name="id" id="deliddata" value="">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="deldatago">Hapus</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
</div><!-- /.modal -->