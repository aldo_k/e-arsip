<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>E-ARSIP<?php if (isset($title)) {
    echo " - " . $title;
  }
  ?></title>

  <!-- Bootstrap Core CSS -->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/flatly.bootstrap.min.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/heroic-features.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/jquery-ui.min.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/jquery-ui.structure.min.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/jquery-ui.theme.min.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/chosen.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/jquery.auto-complete.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/custom.css') ?>" />
  <!-- jQuery -->
  <script src="<?php echo base_url('/public/js/jquery-2.2.2.min.js')?>"></script>
  <script>
    var base_url = '<?php echo base_url(); ?>';
    var site_url = '<?php echo site_url(); ?>';
  </script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <link href="<?php echo base_url('/public/logo.png') ?>" rel="icon" />

    </head>

    <body>

      <!-- Navigation -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#arsip-main-menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a style="padding-top: 20px;" class="navbar-brand" href="<?php echo site_url('/home'); ?>"><span>Aplikasi E-Arsip</span></a>
            <!-- <a style="padding-top: 13px;" class="navbar-brand" href="<?php echo site_url('/home'); ?>"><img src="<?php echo base_url('/public/images/logo-horizontal.png'); ?>" alt="ARSIP" height="35"></a> -->
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="arsip-main-menu">
            <ul class="nav navbar-nav">
             <?php
             if (isset($_SESSION['akses_modul']['arsip_data']) && $_SESSION['akses_modul']['arsip_data'] == 'on') {
              echo '<li><a href="' . site_url('/home') . '"><i class="glyphicon glyphicon-file"></i> Semua Data Arsip</a></li>';
            }
            if (isset($_SESSION['akses_modul']['arsip_tambah']) && $_SESSION['akses_modul']['arsip_tambah'] == 'on') {
              echo '<li><a href="' . site_url('/arsip/tambah') . '"><i class="glyphicon glyphicon-plus"></i> Tambah Arsip Baru</a></li>';
            }
            if ($_SESSION['menu_master']) {
             echo '<li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
             <i class="glyphicon glyphicon-th-large"></i> Data Master <span class="caret"></span></a><ul class="dropdown-menu">';

             if (isset($_SESSION['akses_modul']['pencipta_data']) && $_SESSION['akses_modul']['pencipta_data'] == 'on') {
              echo "<li><a href=\"" . site_url('pencipta') . "\"><i class=\"glyphicon glyphicon-home\"></i> Pencipta arsip</a></li>";
            }
            if (isset($_SESSION['akses_modul']['pengolah_data']) && $_SESSION['akses_modul']['pengolah_data'] == 'on') {
              echo "<li><a href=\"" . site_url('pengolah') . "\"><i class=\"glyphicon glyphicon-home\"></i> Unit Pengolah</a></li>";
            }
            if (isset($_SESSION['akses_modul']['lokasi_data']) && $_SESSION['akses_modul']['lokasi_data'] == 'on') {
              echo "<li><a href=\"" . site_url('lokasi') . "\"><i class=\"glyphicon glyphicon-map-marker\"></i> Lokasi</a></li>";
            }
            if (isset($_SESSION['akses_modul']['pengguna_data']) && $_SESSION['akses_modul']['pengguna_data'] == 'on') {
              echo "<li><a href=\"" . site_url('pengguna') . "\"><i class=\"glyphicon glyphicon-user\"></i> Pengguna</a></li>";
            }
            echo "</ul>
            </li>";
          }
          ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
         <?php
         if (isset($_SESSION['username'])) {
          echo "<li><a href=\"#\"><span class=\"glyphicon glyphicon-user\"></span> " . $_SESSION['username'] . "</a></li>";
          echo "<li><a href=\"" . site_url('/auth/logout') . "\"><span class=\"glyphicon glyphicon-log-out\"></span> Logout</a></li>";
        } else {
          echo "<li><a href=\"" . site_url('home/login') . "\"><span class=\"glyphicon glyphicon-log-in\"></span> Login</a></li>";
        }
        ?>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">