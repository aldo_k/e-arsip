<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-inverse navbar-submenu">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#module-submenu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo site_url('/pengguna'); ?>">Data Pengguna</a>
		</div>
		<form class="navbar-form navbar-left width-half-full" method="get" action="<?php echo site_url('/pengguna'); ?>">
			<div class="input-group width-full">
				<input type="text" name="katakunci" class="form-control" placeholder="kata kunci username" /><span class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button></span>
				</div>
			</form>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="module-submenu">
				<ul class="nav navbar-nav navbar-right">
					<?php if(@$_SESSION['akses_modul']['pengguna_tambah']=='on'){ ?>
						<li><a href="#" data-toggle="modal" data-target="#adduser"><i class="glyphicon glyphicon-plus"></i> Entry Pengguna Baru</a></li>
					<?php } ?>
					<li><a href="javascript:void(0);" id="reloaduser"><i class="glyphicon glyphicon-refresh"></i> Reload Data</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<div class="row">
		<div class="col-md-12 table-responsive" id="divtabeluser">
			<table class="table table-bordered" name="pengguna" id="pengguna">
				<thead>
					<th class="width-sm">No</th>
					<th>Username</th>
					<th>Akses Klasifikasi</th>
					<th class="col-sm-6">Akses Modul</th>
					<?php if(@$_SESSION['akses_modul']['pengguna_edit']=='on'){ ?>
						<th class="width-sm"></th>
					<?php } ?>
					<?php if(@$_SESSION['akses_modul']['pengguna_hapus']=='on'){ ?>
						<th class="width-sm"></th>
					<?php } ?>
				</thead>
				<?php
				if(isset($user)){
					$no=1;
					foreach($user as $u) {
						echo "<tr>";
						echo "<td>".$no."</td>";
						echo "<td>".$u['username']."</td>";
						echo "<td>".$u['akses_klas']."</td>";
						echo "<td>";
						$mm = $u['akses_modul'];
						if($mm!="") {
							$mm = json_decode($mm);
							if($mm) {
								foreach($mm as $key=>$val) {
									echo $key.", ";
								}
							}
						}
						echo "</td>";
						if(@$_SESSION['akses_modul']['pengguna_edit']=='on'){
							echo "<td><a data-toggle=\"modal\" data-target=\"#edituser\" class='eduser' href='#' id='".$u['id']."' title=\"Edit\"><i class=\"glyphicon glyphicon-edit\"></i> </a></td>";
						}
						if(@$_SESSION['akses_modul']['pengguna_hapus']=='on'){
							echo "<td><a data-toggle=\"modal\" data-target=\"#deluser\" class='deluser' href='#' id='".$u['id']."' title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> </a></td>";
						}
						echo "</tr>";
						$no++;
					}
				}
				?>
			</table>
		</div>
	</div>

	<div class="modal fade" id="adduser">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Tambah Pengguna</h4>
				</div>
				<div class="modal-body">
					<form id="fadduser" class="form-horizontal" role="form" method="post" action="<?php echo site_url("/pengguna/adduser"); ?>">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="username">Username</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="username" name="username" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="password">Password</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password" name="password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="conf_password">Konfirmasi password</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="conf_password" name="conf_password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="akses_klas">Hak Akses Klasifikasi</label>
							<div class="col-sm-10">
								<select id="akses_klas" name="akses_klas" class="form-control">
									<option value="Admin">Admin</option>
									<option value="Kepala Seksi" >Kepala Seksi</option>
									<option value="Sekertaris" >Sekertaris</option>
									<option value="Kustom Akses" >Kustom Akses</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="username">Hak Akses Modul</label>
							<div class="col-sm-10">
								<!-- Arsip Akses -->
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="arsip_data" name="modul[arsip_data]">
									Arsip Data
								</label>
								<label class="form-check-label col-sm-8">
									<input class="form-check-input" type="checkbox" id="arsip_download" name="modul[arsip_download]">
									Download Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="arsip_tambah" name="modul[arsip_tambah]">
									Tambah Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="arsip_edit" name="modul[arsip_edit]">
									Edit Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="arsip_hapus" name="modul[arsip_hapus]">
									Hapus Arsip
								</label>

								<!-- Pencipta Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="pencipta_data" name="modul[pencipta_data]">
									Pencipta Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pencipta_tambah" name="modul[pencipta_tambah]">
									Tambah Pencipta 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pencipta_edit" name="modul[pencipta_edit]">
									Edit Pencipta 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pencipta_hapus" name="modul[pencipta_hapus]">
									Hapus Pencipta
								</label>

								<!-- Pengolah Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="pengolah_data" name="modul[pengolah_data]">
									Pengolah Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengolah_tambah" name="modul[pengolah_tambah]">
									Tambah Pengolah
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengolah_edit" name="modul[pengolah_edit]">
									Edit Pengolah
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengolah_hapus" name="modul[pengolah_hapus]">
									Hapus Pengolah
								</label>

								<!-- Lokasi Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="lokasi_data" name="modul[lokasi_data]">
									Lokasi Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="lokasi_tambah" name="modul[lokasi_tambah]">
									Tambah Lokasi
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="lokasi_edit" name="modul[lokasi_edit]">
									Edit Lokasi
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="lokasi_hapus" name="modul[lokasi_hapus]">
									Hapus Lokasi
								</label>
								
								<!-- Pengguna Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="pengguna_data" name="modul[pengguna_data]">
									Pengguna Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengguna_tambah" name="modul[pengguna_tambah]">
									Tambah Pengguna
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengguna_edit" name="modul[pengguna_edit]">
									Edit Pengguna 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="pengguna_hapus" name="modul[pengguna_hapus]">
									Hapus Pengguna 
								</label>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="addusergo">Simpan</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<div class="modal fade" id="edituser">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Edit Pengguna</h4>
				</div>
				<div class="modal-body">
					<form id="feduser" class="form-horizontal" role="form" method="post" action="<?php echo site_url("/pengguna/eduser"); ?>">
						<input type="hidden" name="id" id="ediduser" value="">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="username">Username</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="eusername" name="username" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="password">Password</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="epassword" name="password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="akses_klas">Hak Akses Klasifikasi</label>
							<div class="col-sm-10">
								<select id="eakses_klas" name="akses_klas" class="form-control">
									<option value="Admin" >Admin</option>
									<option value="Kepala Seksi" >Kepala Seksi</option>
									<option value="Sekertaris" >Sekertaris</option>
									<option value="Kustom Akses" >Kustom Akses</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="username">Hak Akses Modul</label>
							<div class="col-sm-10">
								<!-- Arsip Akses -->
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="earsip_data" name="modul[arsip_data]">
									Arsip Data
								</label>
								<label class="form-check-label col-sm-8">
									<input class="form-check-input" type="checkbox" id="earsip_download" name="modul[arsip_download]">
									Download Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="earsip_tambah" name="modul[arsip_tambah]">
									Tambah Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="earsip_edit" name="modul[arsip_edit]">
									Edit Arsip
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="earsip_hapus" name="modul[arsip_hapus]">
									Hapus Arsip
								</label>

								<!-- Pencipta Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="epencipta_data" name="modul[pencipta_data]">
									Pencipta Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epencipta_tambah" name="modul[pencipta_tambah]">
									Tambah Pencipta 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epencipta_edit" name="modul[pencipta_edit]">
									Edit Pencipta 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epencipta_hapus" name="modul[pencipta_hapus]">
									Hapus Pencipta
								</label>

								<!-- Pengolah Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="epengolah_data" name="modul[pengolah_data]">
									Pengolah Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengolah_tambah" name="modul[pengolah_tambah]">
									Tambah Pengolah
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengolah_edit" name="modul[pengolah_edit]">
									Edit Pengolah
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengolah_hapus" name="modul[pengolah_hapus]">
									Hapus Pengolah
								</label>

								<!-- Lokasi Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="elokasi_data" name="modul[lokasi_data]">
									Lokasi Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="elokasi_tambah" name="modul[lokasi_tambah]">
									Tambah Lokasi
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="elokasi_edit" name="modul[lokasi_edit]">
									Edit Lokasi
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="elokasi_hapus" name="modul[lokasi_hapus]">
									Hapus Lokasi
								</label>
								
								<!-- Pengguna Akses -->
								<label class="form-check-label col-sm-12">
									<input class="form-check-input" type="checkbox" id="epengguna_data" name="modul[pengguna_data]">
									Pengguna Data
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengguna_tambah" name="modul[pengguna_tambah]">
									Tambah Pengguna
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengguna_edit" name="modul[pengguna_edit]">
									Edit Pengguna 
								</label>
								<label class="form-check-label col-sm-4">
									<input class="form-check-input" type="checkbox" id="epengguna_hapus" name="modul[pengguna_hapus]">
									Hapus Pengguna 
								</label>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="editusergo">Simpan</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="deluser">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Delete Pengguna</h4>
				</div>
				<div class="modal-body">
					<form id="fdeluser" class="form-horizontal" role="form" method="post" action="<?php echo site_url("/pengguna/deluser"); ?>">
						<h4 class="modal-title">Yakin ingin Hapus data ini?</h4>
						<input type="hidden" name="id" id="deliduser" value="">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="delusergo">Hapus</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<script type="text/javascript">
		$('input[type="checkbox"]').prop('checked', true);

		$('select[name="akses_klas"]').on('click', function(event) {
			event.preventDefault();
			akses_klas = $(this).val();
			if (akses_klas === 'Admin') {
				console.log(akses_klas);
				$('input[type="checkbox"]').prop('checked', true);

			} else if (akses_klas === 'Kepala Seksi') {
				$('input[type="checkbox"]').prop('checked', false);

				$('input[name="modul[arsip_data]"').prop('checked', true);
				$('input[name="modul[arsip_hapus]"').prop('checked', true);
				$('input[name="modul[arsip_download]"').prop('checked', true);

			} else if (akses_klas === 'Sekertaris') {
				$('input[type="checkbox"]').prop('checked', false);

				$('input[name="modul[arsip_data]"').prop('checked', true);
				$('input[name="modul[arsip_edit]"').prop('checked', true);
				$('input[name="modul[arsip_tambah]"').prop('checked', true);
				$('input[name="modul[arsip_download]"').prop('checked', true);

			} else {
				$('input[type="checkbox"]').prop('checked', true);

			}
		});

		$('input[type="checkbox"]').on('click', function(event) {
			// event.preventDefault();
			$('select[name="akses_klas"]').val('Kustom Akses');
			// $('input[type="checkbox"]').clicked();
		});

	</script>