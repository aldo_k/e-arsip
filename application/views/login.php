<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/bootstrap.min.css') ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('/public/css/login.css') ?>" />
  <link href="<?php echo base_url('/public/logo.png') ?>" rel="icon" />
</head>
<body>

  <div class="container" id=logindiv>
   <div class="row">
     <div class="container" id="formContainer">

      <form class="form-signin" id="login" role="form" method="post" action="<?php echo site_url('/auth/gologin'); ?>">
        <h3 align="center">Aplikasi E-Arsip</h3>
        <br>
        <?php
        if($this->session->flashdata('erorlogin')) {
          echo "<div id=\"pesan_id\" style=\"color:red; text-align:center;\">".$this->session->flashdata('erorlogin')."</div>";
        }
        ?>
        <div class="form-group">
          <input type="hidden" name="previous" value="<?php echo (isset($previous)?$previous:"") ?>">
          <label for="">Username</label>
          <input type="text" class="form-control" name="username" id="loginEmail" placeholder="Masukkan Username" required autofocus>

        </div>
        <div>
          <label for="">Password</label>
          <input type="password" class="form-control" name="password" id="loginPass" placeholder="Masukkan Password" required>
        </div>
        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" id="Login" type="submit">Login</button>
        </div>
      </form>

    </div>
  </div>
</div>
<script src="<?php echo base_url('/public/js/jquery-2.2.2.min.js')?>"></script>
<script src="<?php echo base_url('/public/js/bootstrap.min.js')?>"></script>
<script type="text/javascript">
  nilai = $('#hitung_mundur').text();
  if (nilai != '') {
    $('#Login').attr("disabled", true);
    setInterval(function(){ 
      $('#hitung_mundur').text(nilai--); 
      if (nilai==0) {
        $('#pesan_id').text('Silahkan Mencoba Login Kembali...');
        $('#Login').removeAttr("disabled");
      }
    }, 1000);
  }
</script>
</body>
</html>