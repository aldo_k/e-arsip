<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

if (!function_exists('html_purify')) {
  function html_purify($dirty_html, $config = false)
  {
    require_once APPPATH.'third_party/htmlpurifier-4.10.0-standalone/HTMLPurifier.standalone.php';

    if (is_array($dirty_html)) {
      foreach ($dirty_html as $key => $val) {
        $clean_html[$key] = html_purify($val, $config);
      }
    } else {
      $ci = &get_instance();

      switch ($config) {
        case 'comment':
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', $ci->config->item('charset'));
        $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
        $config->set('HTML.Allowed', 'p,a[href|title],abbr[title],acronym[title],b,strong,blockquote[cite],code,em,i,strike');
        $config->set('AutoFormat.AutoParagraph', true);
        $config->set('AutoFormat.Linkify', true);
        $config->set('AutoFormat.RemoveEmpty', true);
        break;

        case false:
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', $ci->config->item('charset'));
        $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
        break;

        default:
        show_error('The HTMLPurifier configuration labeled "'.htmlspecialchars($config, ENT_QUOTES, $ci->config->item('charset')).'" could not be found.');
      }

      $purifier = new HTMLPurifier($config);
      $clean_html = $purifier->purify($dirty_html);
    }

    return $clean_html;
  }
}
 

// source encrypt and decrypt from link https://www.php.net/manual/en/filters.encryption.php
// source encrypt and decrypt from link https://arjunphp.com/encrypt-decrypt-files-using-php/

// source code kedua fungsi ini dihasilkan dari kedua link tersebut dan telah dilakukan improvisasi dengan penyederhanaan fungsi
// dari manual book php dan penambahan sedikit fungsi sha1 pada iv dari arjunphp untuk lebih memperkuat enkripsi
// dengan menambah panjang iv dan key pada fungsi enkripsi dan deskripsi file yg akan digunakan pada class Arsip

if (!function_exists('encrypt_file')) {
  function encrypt_file($file, $destination, $passphrase) {
    // Open the file and returns a file pointer resource. 
    $handle = fopen($file, "rb") or die("Could not open a file."); 
    // Returns the read string.
    $contents = fread($handle, filesize($file));
    // Close the opened file pointer.
    fclose($handle); 

    // source sha1 raw data https://php.net/manual/en/function.sha1.php
    // hash pada key digabung 2x sha1 dikarenakan raw data sha1 hanya berjumlah 20 karakter (untuk mencukupi get 27 karakter pertama)
    // dari hasil penggabungan 2 sha1 tersebut didapatkanlah hasil 40 karakter
    // digunakan untuk iv dan key yang dipotong 32 karakter dan 27 karakter
    // sumber tertera pada https://php.net/manual/en/function.sha1.php bagian parameter
    // 
    // kenapa panjang key 27, itu dikarenakan rinjdael dapat menggunakan key dinamis dari 1 sampai dengan 32 karakter
    // (berdasarkan http://rijndael.online-domain-tools.com/)
    //
    // untuk iv sendiri bervariasi mulai dari max 16 sampai dengan 32 karakter
    // seperti tertera pada website http://rijndael.online-domain-tools.com/

    // untuk mode ekripsi dan deskripsi sendiri, seperti yang telah dijelaskan pada dokumentasi php, default mode ialah CBC
    // disini penulis memilih mode CBC
    // karena CBC melakukan XOR terlebih dahulu sebelum melakukan enkripsi
    // dan hal ini juga membantu membuat sistem enkripsi dan deskripsi menjadi lebih aman
    $iv = substr(sha1("\x1B\x3C\x58".$passphrase, true) . sha1("\x1B\x3C\x58".$passphrase, true), 0, 32);
    $key = substr(sha1("\x2D\xFC\xD8".$passphrase, true) . sha1("\x2D\xFC\xD9". $passphrase, true), 0, 27);
    $opts = array('iv'=>$iv, 'key'=>$key);
    $fp = fopen($destination, 'wb') or die("Could not open file for writing.");
    // Add the Mcrypt stream filter with Rijndael-256
    stream_filter_append($fp, 'mcrypt.rijndael-256', STREAM_FILTER_WRITE, $opts); 
    // Write content in the destination file.
    fwrite($fp, $contents) or die("Could not write to file."); 
    // Close the opened file pointer.
    fclose($fp); 

  }
}

if (!function_exists('decrypt_file')) {
  function decrypt_file($file,$passphrase) {
    $iv = substr(sha1("\x1B\x3C\x58".$passphrase, true) . sha1("\x1B\x3C\x58".$passphrase, true), 0, 32);
    $key = substr(sha1("\x2D\xFC\xD8".$passphrase, true) . sha1("\x2D\xFC\xD9".$passphrase, true), 0, 27);
    $opts = array('iv'=>$iv, 'key'=>$key);
    $fp = fopen($file, 'rb');
    stream_filter_append($fp, 'mdecrypt.rijndael-256', STREAM_FILTER_READ, $opts);
    return $fp;
  }
}