<?php defined('BASEPATH') or exit('No direct script access allowed');

class Arsip extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (@$_SESSION['akses_modul']['arsip_data']!="on") {
      redirect('/auth/index', 'refresh');
    }
  }

  protected function __output($nview, $data = null)
  {
    $this->load->view('header', $data);
    $this->load->view($nview, $data);
    $this->load->view('footer');
  }

  protected function __sanitizeString($str)
  {
    return html_purify($str);
  }

  protected function masterlist($tipe)
  {
    $data;
    switch ($tipe) {
      case "pencipta":
      $q = "SELECT * FROM master_pencipta ORDER BY nama_pencipta ASC";
      $hsl = $this->db->query($q);
      $data = $hsl->result_array();
      break;
      case "unitpengolah":
      $q = "SELECT * FROM master_pengolah ORDER BY nama_pengolah ASC";
      $hsl = $this->db->query($q);
      $data = $hsl->result_array();
      break;
      case "lokasi":
      $q = "SELECT * FROM master_lokasi ORDER BY nama_lokasi ASC";
      $hsl = $this->db->query($q);
      $data = $hsl->result_array();
      break;
    }

    return $data;
  }

  public function tambah()
  {
    if (@$_SESSION['akses_modul']['arsip_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $data["pencipta"] = $this->masterlist("pencipta");
    $data["unitpengolah"] = $this->masterlist("unitpengolah");
    $data["lokasi"] = $this->masterlist("lokasi");
    $data["title"] = "Tambah Arsip";

    $this->__output('entri1', $data);
  }

  public function gentr()
  {
    if (@$_SESSION['akses_modul']['arsip_tambah']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $noarsip = $this->__sanitizeString($this->input->post('noarsip'));
    $tanggal = $this->__sanitizeString($this->input->post('tanggal'));
    $uraian = $this->__sanitizeString($this->input->post('uraian'));
    $pencipta = $this->__sanitizeString($this->input->post('pencipta'));
    $unitpengolah = $this->__sanitizeString($this->input->post('unitpengolah'));
    $lokasi = $this->__sanitizeString($this->input->post('lokasi'));
    $ket = $this->__sanitizeString($this->input->post('ket'));
    $jumlah = $this->__sanitizeString($this->input->post('jumlah'));
    $file = "";
    $config['upload_path'] = 'files/';
    $config['file_name'] = $_FILES["file"]['name'];
    $config['allowed_types'] = 'pdf|docx|doc|jpg|jpeg|png|rar|zip|txt';
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('file')) {
      $datafile = $this->upload->data();
      
      $file         = $datafile['file_name'];
      $key_enkripsi = $datafile['file_name'];
      
      // penjelasan base64_encode https://www.php.net/manual/en/function.base64-encode.php
      // penjelasan base64_decode https://www.php.net/manual/en/function.base64-decode.php
      // penjelasan md5           https://www.php.net/manual/en/function.md5.php

      // fungsi encrypt_file() terketak pada folder application/helpers dengan nama htmlpurifier_helper.php
      encrypt_file($datafile['full_path'],"./files/" . base64_encode(md5($datafile['file_name'])) ,$key_enkripsi);
      // menghapus file asli
      unlink($datafile['full_path']);
    } else {
      echo $this->upload->display_errors();
      echo $config['upload_path'];
      die();
    }

    $q = sprintf("INSERT INTO data_arsip (noarsip,tanggal,uraian,ket,file,jumlah,pencipta,unit_pengolah,lokasi,tgl_input,username)
     VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%d', %d, %d, now(), '%s')",
     $noarsip, $tanggal, $uraian, $ket, $file, $jumlah, $pencipta, $unitpengolah, $lokasi, $_SESSION['username']);
    
    $hsl = $this->db->query($q);
    $q = "SELECT LAST_INSERT_ID() as vid;";
    $hsl = $this->db->query($q);
    $row = $hsl->row_array();
    $v = $row['vid'];

    redirect('/home/view/' . $v, 'refresh');
  }

  public function cekNoArsip(){
    $status = $this->__sanitizeString($this->input->post('status'));
    if ($status=='tambah') {
      $noarsip = $this->__sanitizeString($this->input->post('noarsip'));
      $q = "SELECT COUNT(*) AS jumlah FROM data_arsip WHERE noarsip='$noarsip'";
    } else if ($status=='edit') {
      $noarsip_old = $this->__sanitizeString($this->input->post('noarsip_old'));
      $noarsip = $this->__sanitizeString($this->input->post('noarsip'));
      $q = "SELECT COUNT(*) AS jumlah FROM data_arsip WHERE noarsip='$noarsip' AND noarsip!='$noarsip_old'";
    }
    $d = $this->db->query($q)->row_array()['jumlah'];
    echo $jumlah = $d>0;
  }

  public function download($filename='')
  {
    if (@$_SESSION['akses_modul']['arsip_download']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $decrypted = decrypt_file("./files/" . base64_encode(md5($filename)), $filename);
      // header('Content-type: application/pdf');
    header('Content-disposition: attachment; filename='.$filename);
    header('Content-type: application/octet-stream');
      // header('Content-Length: '. strlen($decrypted));
    header("Pragma: no-cache");
    header("Expires: 0");
    fpassthru($decrypted);
  }

  public function perbarui($id)
  {
    if (@$_SESSION['akses_modul']['arsip_edit']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    if ($id != "") {
      $q = sprintf("SELECT * FROM data_arsip WHERE id=%d", $id);
      $hsl = $this->db->query($q);
      $row = $hsl->row_array();
      $previous = "";
      if (isset($_SERVER['HTTP_REFERER'])) {
        $previous = $_SERVER['HTTP_REFERER'];
        $row['previous'] = $previous;
      }
      $row["pencipta2"] = $this->masterlist("pencipta");
      $row["unitpengolah2"] = $this->masterlist("unitpengolah");
      $row["lokasi2"] = $this->masterlist("lokasi");
      $row["title"] = "Ubah Arsip";
      if (count($row) > 0) {
        $this->__output('edit1', $row);
      } else {
        redirect('/home/', 'refresh');
      }
    } else {
      redirect('/home/', 'refresh');
    }
  }

  public function edit()
  {
    if (@$_SESSION['akses_modul']['arsip_edit']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $noarsip = $this->__sanitizeString($this->input->post('noarsip'));
    $tanggal = $this->__sanitizeString($this->input->post('tanggal'));
    $uraian = $this->__sanitizeString($this->input->post('uraian'));
    $ket = $this->__sanitizeString($this->input->post('ket'));
    $pencipta = $this->__sanitizeString($this->input->post('pencipta'));
    $unitpengolah = $this->__sanitizeString($this->input->post('unitpengolah'));
    $lokasi = $this->__sanitizeString($this->input->post('lokasi'));
    $jumlah = $this->__sanitizeString($this->input->post('jumlah'));
    $id = $this->__sanitizeString($this->input->post('id'));
    $previous = $this->__sanitizeString($this->input->post('previous'));
    $file = "";
    $config['upload_path'] = 'files/';
    $config['file_name'] = $_FILES["file"]['name'];
    $config['allowed_types'] = 'pdf|docx|doc|jpg|jpeg|png|rar|zip|txt';
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('file')) {
      // mencari nama file lama untuk kemudian dihapus
      $q = "SELECT file FROM data_arsip WHERE id=$id";
      $hsl = $this->db->query($q);
      $row = $hsl->row_array()['file'];
      if ($row != "") {
        $alamat = ROOTPATH . "/files/" . base64_encode(md5($row));
      // menghapus file lama
        unlink($alamat);
      }

      $datafile = $this->upload->data();
      $file = $datafile['file_name'];

      $key_enkripsi = $datafile['file_name'];

      // penjelasan base64_encode https://www.php.net/manual/en/function.base64-encode.php
      // penjelasan base64_decode https://www.php.net/manual/en/function.base64-decode.php
      // penjelasan md5           https://www.php.net/manual/en/function.md5.php

      // fungsi encrypt_file() terketak pada folder application/helpers dengan nama htmlpurifier_helper.php
      encrypt_file($datafile['full_path'], "./files/" . base64_encode(md5($datafile['file_name'])) ,$key_enkripsi);
      
      // untuk menghapus file asli
      unlink($datafile['full_path']);
    } else {
      $q = "SELECT file FROM data_arsip WHERE id=$id";
      $d = $this->db->query($q)->row_array()['file'];
      $file = $d;
    }

    if (isset($_POST)) {
      $q = sprintf("UPDATE data_arsip SET noarsip='%s',tanggal='%s',uraian='%s', ket='%s',file='%s',jumlah='%d', pencipta=%d,unit_pengolah=%d,lokasi=%d WHERE id=$id",
       $noarsip, $tanggal, $uraian, $ket, $file, $jumlah,$pencipta, $unitpengolah, $lokasi);
      $hsl = $this->db->query($q);
    }
    redirect('/home/view/' . $id, 'refresh');
  }

  public function hapus_file()
  {
    if (@$_SESSION['akses_modul']['arsip_edit']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $id = $this->__sanitizeString($this->input->post('id'));
    $q = "SELECT file FROM data_arsip WHERE id=$id";
    $hsl = $this->db->query($q);
    $row = $hsl->row_array()['file'];
    if ($row != "") {
      $alamat = ROOTPATH . "/files/" . base64_encode(md5($row));
      unlink($alamat);
    }
    $q = sprintf("UPDATE data_arsip SET file=NULL WHERE id=%d", $id);
    $hsl = $this->db->query($q);
  }

  public function hapus_arsip()
  {
    if (@$_SESSION['akses_modul']['arsip_hapus']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("SELECT file FROM data_arsip WHERE id=%d", $id);
    $hsl = $this->db->query($q);
    $row = $hsl->row_array()['file'];
    if ($row != "") {
      $alamat = ROOTPATH . "/files/" . base64_encode(md5($row));
      unlink($alamat);
    }
    $q = sprintf("DELETE FROM data_arsip WHERE id=%d", $id);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }
}
