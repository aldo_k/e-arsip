<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	private static $timeFrame = 10;

	private static $threshold = array(
		2 => 60,
		3 => 120,
		5 => 180,
	);

	public function __construct()
	{
		parent::__construct();

		if(!@$_SESSION['username'] && $this->uri->segment(2)!='index' && $this->uri->segment(2)!='gologin') {
			redirect('/auth/index', 'refresh');
		}
	}

	protected function __sanitizeString($str)
	{
		return html_purify($str);
	}
	public function index()
	{
		$delay = self::getLoginDelay();
		if($delay > 0)
		{
			$this->session->set_flashdata('erorlogin', "Anda Terlalu Banyak Mencoba, Silahkan Coba <span id='hitung_mundur'>$delay</span> detik Lagi.");
		} else if ($this->session->flashdata('erorlogin')!='Username atau password yang ada masukkan salah') {
			$this->session->set_flashdata('erorlogin', "Login to Dashboard");
		}

		$data=[];
		if(isset($_SERVER['HTTP_REFERER'])) {
			$previous = $_SERVER['HTTP_REFERER'];
			$data['previous'] = $previous;
		}
		$this->load->view('login',$data);
	}

	public function gologin()
	{
		$delay = self::getLoginDelay();
		if($delay > 0)
		{
			$username=$this->__sanitizeString($this->input->post('username'));
			if ($username!='') {
				self::addFailedLogin($username);
				// memeriksa kembali nilai delay login
				$delay = self::getLoginDelay();
			}
			$this->session->set_flashdata('erorlogin', "Anda Terlalu Banyak Mencoba, Silahkan Coba <span id='hitung_mundur'>$delay</span> detik Lagi.");
			redirect('/auth/index', 'refresh');
		} else {
			$username=$this->__sanitizeString($this->input->post('username'));
			$password=$this->input->post('password');
			$previous=$this->__sanitizeString($this->input->post('previous'));
			$q = "SELECT * FROM master_user WHERE username='$username'";
			$user = $this->db->query($q)->row();

			if($user && (password_verify($password, $user->password))) {
				$_SESSION['username'] = $username;
				$_SESSION['id_user'] = $user->id;
				$_SESSION['akses_klas'] = $user->akses_klas;
				$_SESSION['akses_modul'] = json_decode($user->akses_modul,true);
				$_SESSION['menu_master'] = false;
				if(count($_SESSION['akses_modul'])>0) {
					$no=0;
					foreach($_SESSION['akses_modul'] as $key=>$val) {
						if($key=='pencipta_data') $no++;
						if($key=='pengolah_data') $no++;
						if($key=='lokasi_data') $no++;
						if($key=='pengguna_data') $no++;
					}
					if($no>0) {
						$_SESSION['menu_master'] = true;
					}
				}
				$ip 			= self::getIP();
				$username = trim($this->input->post('username'));
				// menghapus log pada IP / Username yang berhasil login atau data gagal login yang bukan hari ini untuk membersihkan table user_failed_login
				$q  			= 'DELETE FROM user_failed_login where username=? OR ip=? OR date(`timestamp`)
				!= curdate()';
				$hsl 			= $this->db->query($q, array($username, $ip));
				if($previous=="") {
					redirect('/home', 'refresh');
				} else {
					header('Location: ' . $previous);
				}
			} else {
				self::addFailedLogin($username);

				$this->session->set_flashdata('erorlogin', 'Username atau password yang ada masukkan salah');
				redirect('/auth/index', 'refresh');
			}
		}
	}

	private static function getIP()
	{
		$ipaddress = '';
		if(getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	public function addFailedLogin($username="anonymous")
	{
		$q = sprintf("INSERT INTO user_failed_login(`username`,`ip`, `timestamp`) VALUES ('%s', '%s', NOW())", $username, self::getIP());

		$hsl = $this->db->query($q);
	}

	public function getLoginDelay($filter = 'IP', $username="anonymous")
	{
		if ($this->input->post('username')!='') {
			$username = $this->input->post('username');
			$filter   = 'BOTH';
		}
    // Get number of failed attempts and timestap for last failed attempt
		$where = "";
		if(strtoupper($filter) == 'IP')
		{
			$where = sprintf("ip = '%s'", self::getIP());
		}
		if(strtoupper($filter) == 'USERNAME')
		{
			$where = sprintf("username = '%s'", $username);
		}
		if(strtoupper($filter) == 'BOTH')
		{
			$where = sprintf("ip = '%s' AND username = '%s'", self::getIP(), $username);
		}
		$stmt = sprintf("SELECT COUNT(id) as `count`, MAX(timestamp) as `lastDate` FROM user_failed_login WHERE $where AND timestamp > DATE_SUB(NOW(), INTERVAL %s MINUTE)", self::$timeFrame);

		$hsl = $this->db->query($stmt);
		$row = $hsl->result_array();

    // Get count of last failed logins
		$failedAttempts = (int) $row[0]['count'];

    // Get timestamp of last failed login
		$lastFailedTimestamp = strtotime($row[0]['lastDate']);
		krsort(self::$threshold);
		foreach (self::$threshold as $attempts => $delay){

			if ($failedAttempts > $attempts && time() < ($lastFailedTimestamp+$delay))
				return ($lastFailedTimestamp+$delay) - time();
			return 0;
		}
	}


	public function logout()
	{
		unset($_SESSION['username']);
		unset($_SESSION['id_user']);
		unset($_SESSION['tipe']);
		unset($_SESSION['akses_klas']);
		unset($_SESSION['akses_modul']);
		unset($_SESSION['menu_master']);
		redirect('/auth', 'refresh');
	}

}






