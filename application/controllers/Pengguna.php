<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    if (!$_SESSION['menu_master']) {
      redirect('/auth/index', 'refresh');
    }
  }

  protected function __output($nview, $data = null)
  {
    $this->load->view('header', $data);
    $this->load->view($nview, $data);
    $this->load->view('footer');
  }

  protected function __sanitizeString($str)
  {
    return html_purify($str);
  }

  public function index()
  {
    if (@$_SESSION['akses_modul']['pengguna_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $katakunci = $this->__sanitizeString($this->input->get('katakunci'));

    $q = "SELECT * FROM master_user ";
    if ($katakunci) {
      $q .= ' WHERE username LIKE \'%' . $katakunci . '%\' ';
    }
    $q .= " ORDER BY username ASC";
    $hsl = $this->db->query($q);
    $data['user'] = $hsl->result_array();
    $this->__output('pengguna', $data);
  }

  public function cekuser()
  {
    if (@$_SESSION['akses_modul']['pengguna_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $username = $this->__sanitizeString($this->input->post('username'));
    $q = "SELECT username FROM master_user WHERE username='$username'";
    $hsl = $this->db->query($q)->row_array();
    if ($hsl['username'] == $username) {
      echo json_encode(array('msg' => 'error'));
    } else {
      echo json_encode(array('msg' => 'ok'));
    }
  }

  
  public function adduser()
  {
    if (@$_SESSION['akses_modul']['pengguna_tambah']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $password_str = $this->input->post('password');
    $conf_password_str = $this->input->post('conf_password');
    if ($password_str !== $conf_password_str) {
      echo json_encode(array('status' => 'error', 'pesan' => 'PASSWORD_UNMATCH'));exit();
    }

    $username = $this->__sanitizeString($this->input->post('username'));
    $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
    $akses_klas = $this->__sanitizeString($this->input->post('akses_klas'));
    $akses_modul = json_encode($this->input->post('modul'));
    $q = sprintf("INSERT INTO master_user (username,password,akses_klas,akses_modul) VALUES ('%s', '%s', '%s','%s')",
      $username, $password, $akses_klas, $akses_modul);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }

  public function eduser()
  {
    if (@$_SESSION['akses_modul']['pengguna_edit']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $username = $this->__sanitizeString($this->input->post('username'));
    $password = "";
    if ($this->input->post('password') != "") {
      $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
    }
    $akses_klas = $this->__sanitizeString($this->input->post('akses_klas'));
    $akses_modul = json_encode($this->input->post('modul'));
    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("UPDATE master_user SET username='%s'", $username);
    if ($password != "") {
      $q .= sprintf(",password='%s'", $password);
    }

    $q .= sprintf(",akses_klas='%s',akses_modul='%s' WHERE id=%d",
      $akses_klas, $akses_modul, $id);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }

  public function deluser()
  {
    if (@$_SESSION['akses_modul']['pengguna_hapus']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("DELETE FROM master_user WHERE id=%d", $id);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }

  public function auser()
  {
    if (@$_SESSION['akses_modul']['pengguna_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("SELECT * FROM master_user WHERE id=%d", $id);
    $hsl = $this->db->query($q);
    $row = $hsl->row_array();
    if ($row) {
      echo json_encode($row);
    } else {
      echo '[]';
    }
    exit();
  }

  
  public function reloaduser()
  {
    if (@$_SESSION['akses_modul']['pengguna_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }
    $q = "SELECT * FROM master_user";
    $hsl = $this->db->query($q);
    $row = $hsl->result_array();
    if ($row) {
      echo "<table class='table table-bordered' name='pengguna' id='pengguna'>
      <thead>
      <th class='width-sm'>No</th>
      <th>Username</th>
      <th>Akses Klasifikasi</th>
      <th class=\"col-sm-6\">Akses Modul</th>";
      if(@$_SESSION['akses_modul']['pengguna_edit']=='on'){
        echo "<th class='width-sm'></th>";
      }
      if(@$_SESSION['akses_modul']['pengguna_hapus']=='on'){
        echo "<th class='width-sm'></th>";
      }
      echo "</thead>";
      $no = 1;
      foreach ($row as $u) {
        echo "<tr>";
        echo "<td>" . $no . "</td>";
        echo "<td>" . $u['username'] . "</td>";
        echo "<td>" . $u['akses_klas'] . "</td>";
        echo "<td>";
        $mm = $u['akses_modul'];
        if ($mm != "") {
          $mm = json_decode($mm);
          if ($mm) {
            foreach ($mm as $key => $val) {
              echo $key . ", ";
            }
          }
        }
        echo "</td>";
        if(@$_SESSION['akses_modul']['pengguna_edit']=='on'){
          echo "<td><a data-toggle=\"modal\" data-target=\"#edituser\" class='eduser' href='#' id='" . $u['id'] . "' title=\"Edit\"><i class=\"glyphicon glyphicon-edit\"></i> </a></td>";
        }
        if(@$_SESSION['akses_modul']['pengguna_hapus']=='on'){
          echo "<td><a data-toggle=\"modal\" data-target=\"#deluser\" class='deluser' href='#' id='" . $u['id'] . "' title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> </a></td>";
        }
        echo "</tr>";
        $no++;
      }
      echo "</table>";
    }
  }
}