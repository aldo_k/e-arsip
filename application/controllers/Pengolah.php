<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pengolah extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$_SESSION['menu_master']) {
			redirect('/auth/index', 'refresh');
		}
	}

	protected function __output($nview, $data = null)
	{
		$this->load->view('header', $data);
		$this->load->view($nview, $data);
		$this->load->view('footer');
	}

	protected function __sanitizeString($str)
	{
		return html_purify($str);
	}

	
	public function index()
	{
		if (@$_SESSION['akses_modul']['pengolah_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}
		$katakunci = $this->__sanitizeString($this->input->get('katakunci'));

		$q = "SELECT * FROM master_pengolah ";
		if ($katakunci) {
			$q .= ' WHERE nama_pengolah LIKE \'%' . $katakunci . '%\' OR id LIKE \'%' . $katakunci . '%\' ';
		}
		$q .= " ORDER BY nama_pengolah ASC";
		$hsl = $this->db->query($q);
		$data['peng'] = $hsl->result_array();
		$this->__output('pengolah', $data);
	}

	public function addpeng()
	{
		if (@$_SESSION['akses_modul']['pengolah_tambah']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$nama = $this->__sanitizeString($this->input->post('nama'));
		$q = sprintf("INSERT INTO master_pengolah (nama_pengolah) VALUES ('%s')", $nama);
		$hsl = $this->db->query($q);
		if ($hsl) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo '[]';
		}
		exit();
	}

	public function edpeng()
	{
		if (@$_SESSION['akses_modul']['pengolah_edit']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$nama = $this->__sanitizeString($this->input->post('nama'));
		$id = $this->__sanitizeString($this->input->post('id'));
		$q = sprintf("UPDATE master_pengolah SET nama_pengolah='%s'", $nama);
		$q .= " WHERE id=$id";
		$hsl = $this->db->query($q);
		if ($hsl) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo '[]';
		}
		exit();
	}

	public function delpeng()
	{
		if (@$_SESSION['akses_modul']['pengolah_hapus']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}
		$id = $this->__sanitizeString($this->input->post('id'));
        //cek dulu apakah ada arsip yang menggunakan unit pengolah ini
		$q = sprintf("SELECT count(id) jml FROM data_arsip WHERE unit_pengolah=%d", $id);
		$jml = $this->db->query($q)->row_array()['jml'];
		if ($jml == 0) {
			$q = sprintf("DELETE FROM master_pengolah WHERE id=%d", $id);
			$hsl = $this->db->query($q);
			if ($hsl) {
				echo json_encode(array('status' => 'success'));
			} else {
				echo '[]';
			}
			exit();
		} else {

		}
	}

	public function apeng()
	{
		if (@$_SESSION['akses_modul']['pengolah_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$id = $this->__sanitizeString($this->input->post('id'));
		$q = sprintf("SELECT * FROM master_pengolah WHERE id=%d", $id);
		$hsl = $this->db->query($q);
		$row = $hsl->row_array();
		if ($row) {
			echo json_encode($row);
		} else {
			echo '[]';
		}
		exit();
	}

	public function reloadpeng()
	{
		if (@$_SESSION['akses_modul']['pengolah_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$q = "SELECT * FROM master_pengolah ORDER BY nama_pengolah ASC";
		$hsl = $this->db->query($q);
		$row = $hsl->result_array();
		if ($row) {
			echo "<table class='table table-bordered' name='vpeng' id='vpeng'>
			<thead>
			<th class='width-sm'>No</th>
			<th>Nama</th>";
			if(@$_SESSION['akses_modul']['pengolah_edit']=='on'){
				echo "<th class='width-sm'></th>";
			}
			if(@$_SESSION['akses_modul']['pengolah_hapus']=='on'){
				echo "<th class='width-sm'></th>";
			}
			echo "</thead>";
			$no = 1;
			foreach ($row as $u) {
				echo "<tr>";
				echo "<td>" . $no . "</td>";
				echo "<td>" . $u['nama_pengolah'] . "</td>";
				if(@$_SESSION['akses_modul']['pengolah_edit']=='on'){
					echo "<td><a data-toggle=\"modal\" data-target=\"#editpeng\" class='edpeng' href='#' id='" . $u['id'] . "' title=\"Edit\"><i class=\"glyphicon glyphicon-edit\"></i> </a></td>";
				}
				if(@$_SESSION['akses_modul']['pengolah_hapus']=='on'){
					echo "<td><a data-toggle=\"modal\" data-target=\"#delpeng\" class='delpeng' href='#' id='" . $u['id'] . "' title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> </a></td>";
				}
				echo "</tr>";
				$no++;
			}
			echo "</table>";
		}
	}
}