<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	private $data_per_page = 20;

	public function __construct()
	{
		parent::__construct();

		if(!@$_SESSION['username'] && $this->uri->segment(2)!='login' && $this->uri->segment(2)!='gologin') {
			redirect('/auth/index', 'refresh');
		}
	}

	protected function __output($nview,$data=null)
	{
		$this->load->view('header',$data);
		$this->load->view($nview,$data);
		$this->load->view('footer');
	}

	protected function __sanitizeString($str)
	{
		return html_purify($str);
	}

	protected function src($srcdata=false)
	{
		$katakunci=$this->__sanitizeString($this->input->get('katakunci'));
		$noarsip=$this->__sanitizeString($this->input->get('noarsip'));
		$tanggal=$this->__sanitizeString($this->input->get('tanggal'));
		$uraian=$this->__sanitizeString($this->input->get('uraian'));
		$ket=$this->__sanitizeString($this->input->get('ket'));
		$retensi=$this->__sanitizeString($this->input->get('retensi'));
		$pencipta=$this->__sanitizeString($this->input->get('pencipta'));
		$peng=$this->__sanitizeString($this->input->get('peng'));
		$lok=$this->__sanitizeString($this->input->get('lok'));

		$w = array();
		$klasifikasi = array();
		if ($katakunci) {
			$w[] = " noarsip like '%".$katakunci."%'";
			$w[] = " uraian like '%".$katakunci."%'";
		} else {
			if($noarsip!="") {
				$w[] = " noarsip like '%".$noarsip."%'";
			}
			if($tanggal!="") {
				$w[] = " tanggal like '%".$tanggal."%'";
			}
			if($ket!="" && $ket!="all") {
				$w[] = " ket='".$ket."'";
			}
			if($uraian!="") {
				$w[] = " uraian like '%".$uraian."%'";
			}
			if($retensi!="" && $retensi!="all") {
				$w[] = " f='".$retensi."'";
			}
			if($pencipta!="" && $pencipta!="all") {
				$w[] = " pencipta ='".$pencipta."'";
			}
			if($peng!="" && $peng!="all") {
				$w[] = " unit_pengolah ='".$peng."'";
			}
			if($lok!="" && $lok!="all") {
				$w[] = " lokasi ='".$lok."'";
			}
		}

		$q = "SELECT a.*, nama_lokasi,nama_pencipta,nama_pengolah
		FROM data_arsip AS a
		JOIN master_lokasi AS l ON l.id=a.lokasi
		JOIN master_pencipta AS p ON p.id=a.pencipta
		JOIN master_pengolah AS pn ON pn.id=a.unit_pengolah
		";

		$q_count = "SELECT COUNT(*) AS jmldata
		FROM data_arsip AS a
		JOIN master_lokasi AS l ON l.id=a.lokasi
		JOIN master_pencipta AS p ON p.id=a.pencipta
		JOIN master_pengolah AS pn ON pn.id=a.unit_pengolah";
		if($_SESSION['akses_klas']!='') {
			$k = explode(',',$_SESSION['akses_klas']);
			$k = array_filter($k);
			sort($k);
			if(count($k)>0) {
				$klasifikasi=array_merge($klasifikasi,$k);
			}
		}

		if ($katakunci) {
			$q .= " WHERE".implode(" OR ",$w);
			$q_count .= " WHERE".implode(" OR ",$w);
			$src = array("noarsip"=>$katakunci,"tanggal"=>'',"uraian"=>$katakunci,"ket"=>'',"kode"=>'',"retensi"=>'',"pencipta"=>'',"peng"=>'',"lok"=>'',"med"=>'');
			$qq = array($q, $q_count, $src);
			return $qq;
		} else {
			if(count($w) > 0) {
				$q .= " WHERE".implode(" AND ",$w);
				$q_count .= " WHERE".implode(" AND ",$w);
			}
		}

		if(!$katakunci && $srcdata) {
			$src = array("noarsip"=>$noarsip,"tanggal"=>$tanggal,"uraian"=>$uraian,"ket"=>$ket,"retensi"=>$retensi,"pencipta"=>$pencipta,"peng"=>$peng,"lok"=>$lok);
			return array($q, $q_count, $src);
		} else {
			$src = array("Kata kunci"=>$katakunci);
			return array($q, $q_count, $src);
		}
	}

	public function index()
	{
		$this->search();
	}

	public function search($offset=0)
	{
		$qq = $this->src(true);
		$q = $qq[0];
		$data['src']=$qq[2];

		$q2 = $qq[1];
		$q .= " LIMIT $this->data_per_page ";

		$data['current_page'] = 1;
		if ($offset>=$this->data_per_page) {
			$data['current_page'] = floor(($offset+$this->data_per_page)/$this->data_per_page);
		}
		
		if ($offset>0) $q .= "OFFSET $offset";

		$hsl = $this->db->query($q);
		$data['data'] = $hsl->result_array();

		$jmldata = $this->db->query($q2)->row()->jmldata;
		$data['jml']=$jmldata;

		$q = "select distinct ket from data_arsip order by ket asc";
		$hsl = $this->db->query($q);
		$data['ket'] = $hsl->result_array();
		$q = "select * from master_pencipta order by nama_pencipta asc";
		$hsl = $this->db->query($q);
		$data['pencipta'] = $hsl->result_array();
		$q = "select * from master_pengolah order by nama_pengolah asc";
		$hsl = $this->db->query($q);
		$data['peng'] = $hsl->result_array();
		$q = "select * from master_lokasi order by nama_lokasi asc";
		$hsl = $this->db->query($q);
		$data['lok'] = $hsl->result_array();

		$this->load->library('pagination');
		$config['base_url'] = site_url('/home/search/');
		$config['reuse_query_string'] = true;
		$config['total_rows'] = $jmldata;
		$config['per_page'] = $this->data_per_page;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript: void(0)" disabled>';
		$config['cur_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($config);
		$data['pages']=$this->pagination->create_links();

		$this->__output('main',$data);
	}

	public function dl()
	{
		$q = $this->src();
		$hsl = $this->db->query($q[0]);
		$data = $hsl->result_array();
		$this->load->library('excel');
  	//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
  	//name the worksheet
  	//$this->excel->getActiveSheet()->setTitle('test worksheet');
  	//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Data Arsip');
  	//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
  	//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
  	//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
  	//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'No.');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'No.Arsip');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'Tanggal');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'Uraian');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'Pencipta');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'Pengolah');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'Lokasi');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'Ket');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'Jumlah');

		$row=3;
		$no=1;
		foreach($data as $d) {
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $no);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $d['noarsip']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $d['tanggal']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $d['uraian']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $d['nama_pencipta']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $d['nama_pengolah']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $d['nama_lokasi']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $d['ket']);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $d['jumlah']);
			$row++;
			$no++;
		}

  	$filename='Data-Arsip-'.getdate()[0].'.xls'; //save our workbook as this file name
  	header('Content-Type: application/vnd.ms-excel'); //mime type
  	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
  	header('Cache-Control: max-age=0'); //no cache
  	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
  	$objWriter->save('php://output');
  }


  public function view($id)
  {
  	$q="SELECT a.*,p.nama_pencipta,p2.nama_pengolah,l.nama_lokasi
  	FROM data_arsip a
  	LEFT JOIN master_pencipta p ON p.id=a.pencipta
  	LEFT JOIN master_pengolah p2 ON p2.id=a.unit_pengolah
  	LEFT JOIN master_lokasi l ON l.id=a.lokasi
  	WHERE a.id=$id";
  	$data=$this->db->query($q)->row_array();

  	$this->__output('varsip',$data);
  }
}






