<?php defined('BASEPATH') or exit('No direct script access allowed');

class Lokasi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$_SESSION['menu_master']) {
			redirect('/auth/index', 'refresh');
		}
	}

	protected function __output($nview, $data = null)
	{
		$this->load->view('header', $data);
		$this->load->view($nview, $data);
		$this->load->view('footer');
	}

	protected function __sanitizeString($str)
	{
		return html_purify($str);
	}


	public function index()
	{
		if (@$_SESSION['akses_modul']['lokasi_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$katakunci = $this->__sanitizeString($this->input->get('katakunci'));

		$q = "SELECT * FROM master_lokasi ";
		if ($katakunci) {
			$q .= ' WHERE nama_lokasi LIKE \'%' . $katakunci . '%\' OR id LIKE \'%' . $katakunci . '%\' ';
		}
		$q .= " ORDER BY nama_lokasi ASC";
		$hsl = $this->db->query($q);
		$data['lok'] = $hsl->result_array();
		$this->__output('lokasi', $data);
	}

	public function addlok()
	{
		if (@$_SESSION['akses_modul']['lokasi_tambah']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$nama = $this->__sanitizeString($this->input->post('nama'));
		$q = sprintf("INSERT INTO master_lokasi (nama_lokasi) VALUES ('%s')", $nama);
		$hsl = $this->db->query($q);
		if ($hsl) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo '[]';
		}
		exit();
	}

	public function edlok()
	{
		if (@$_SESSION['akses_modul']['lokasi_edit']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$nama = $this->__sanitizeString($this->input->post('nama'));
		$id = $this->__sanitizeString($this->input->post('id'));
		$q = sprintf("UPDATE master_lokasi SET nama_lokasi='%s' WHERE id=%d", $nama, $id);
		$hsl = $this->db->query($q);
		if ($hsl) {
			echo json_encode(array('status' => 'success'));
		} else {
			echo '[]';
		}
		exit();
	}

	public function dellok()
	{
		if (@$_SESSION['akses_modul']['lokasi_hapus']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$id = $this->__sanitizeString($this->input->post('id'));
        //cek dulu apakah ada arsip yang menggunakan lokasi ini
		$q = sprintf("SELECT count(id) jml FROM data_arsip WHERE lokasi=%d", $id);
		$jml = $this->db->query($q)->row_array()['jml'];
		if ($jml == 0) {
			$q = sprintf("DELETE FROM master_lokasi WHERE id=%d", $id);
			$hsl = $this->db->query($q);
			if ($hsl) {
				echo json_encode(array('status' => 'success'));
			} else {
				echo '[]';
			}
			exit();
		} else {

		}
	}

	public function alok()
	{
		if (@$_SESSION['akses_modul']['lokasi_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$id = $this->__sanitizeString($this->input->post('id'));
		$q = "SELECT * FROM master_lokasi WHERE id=$id";
		$hsl = $this->db->query($q);
		$row = $hsl->row_array();
		if ($row) {
			echo json_encode($row);
		} else {
			echo '[]';
		}
		exit();
	}

	public function reloadlok()
	{
		if (@$_SESSION['akses_modul']['lokasi_data']!="on") {
			redirect('/auth/index', 'refresh');
			exit();
		}

		$q = "SELECT * FROM master_lokasi ORDER BY nama_lokasi ASC";
		$hsl = $this->db->query($q);
		$row = $hsl->result_array();
		if ($row) {
			echo "<table class='table table-bordered' name='vlok' id='vlok'>
			<thead>
			<th class='width-sm'>No</th>
			<th>Nama</th>";
			if(@$_SESSION['akses_modul']['lokasi_edit']=='on'){
				echo "<th class='width-sm'></th>";
			}
			if(@$_SESSION['akses_modul']['lokasi_hapus']=='on'){
				echo "<th class='width-sm'></th>";
			}
			echo "</thead>";
			$no = 1;
			foreach ($row as $u) {
				echo "<tr>";
				echo "<td>" . $no . "</td>";
				echo "<td>" . $u['nama_lokasi'] . "</td>";
				if(@$_SESSION['akses_modul']['lokasi_edit']=='on'){
					echo "<td><a data-toggle=\"modal\" data-target=\"#editlok\" class='edlok' href='#' id='" . $u['id'] . "' title=\"Edit\"><i class=\"glyphicon glyphicon-edit\"></i> </a></td>";
				}
				if(@$_SESSION['akses_modul']['lokasi_hapus']=='on'){
					echo "<td><a data-toggle=\"modal\" data-target=\"#dellok\" class='dellok' href='#' id='" . $u['id'] . "' title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> </a></td>";
				}
				echo "</tr>";
				$no++;
			}
			echo "</table>";
		}
	}

}