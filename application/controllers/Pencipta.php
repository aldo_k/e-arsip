<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pencipta extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    if (!$_SESSION['menu_master']) {
      redirect('/auth/index', 'refresh');
    }
  }

  protected function __output($nview, $data = null)
  {
    $this->load->view('header', $data);
    $this->load->view($nview, $data);
    $this->load->view('footer');
  }

  protected function __sanitizeString($str)
  {
    return html_purify($str);
  }

  public function index()
  {
    if (@$_SESSION['akses_modul']['pencipta_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $katakunci = $this->__sanitizeString($this->input->get('katakunci'));

    $q = "SELECT * FROM master_pencipta ";
    if ($katakunci) {
      $q .= ' WHERE nama_pencipta LIKE \'%' . $katakunci . '%\' OR id LIKE \'%' . $katakunci . '%\' ';
    }
    $q .= " ORDER BY nama_pencipta ASC";
    $hsl = $this->db->query($q);
    $data['pencipta'] = $hsl->result_array();
    $this->__output('pencipta', $data);
  }

  public function addpenc()
  {
    if (@$_SESSION['akses_modul']['pencipta_tambah']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $nama = $this->__sanitizeString($this->input->post('nama'));
    $q = sprintf("INSERT INTO master_pencipta (nama_pencipta) VALUES ('%s')", $nama);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }

  public function edpenc()
  {
    if (@$_SESSION['akses_modul']['pencipta_edit']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $nama = $this->__sanitizeString($this->input->post('nama'));
    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("UPDATE master_pencipta SET nama_pencipta='%s' WHERE id=%d", $nama, $id);
    $hsl = $this->db->query($q);
    if ($hsl) {
      echo json_encode(array('status' => 'success'));
    } else {
      echo '[]';
    }
    exit();
  }

  public function delpenc()
  {
    if (@$_SESSION['akses_modul']['pencipta_hapus']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $id = $this->__sanitizeString($this->input->post('id'));
        //cek dulu apakah ada arsip yang menggunakan pencipta ini
    $q = sprintf("SELECT count(id) jml FROM data_arsip WHERE pencipta=%d", $id);
    $jml = $this->db->query($q)->row_array()['jml'];
    if ($jml == 0) { 
      $q = sprintf("DELETE FROM master_pencipta WHERE id=%d", $id);
      $hsl = $this->db->query($q);
      if ($hsl) {
        echo json_encode(array('status' => 'success'));
      } else {
        echo '[]';
      }
      exit();
    } else {

    }
  }

  public function apenc()
  {
    if (@$_SESSION['akses_modul']['pencipta_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $id = $this->__sanitizeString($this->input->post('id'));
    $q = sprintf("SELECT * FROM master_pencipta WHERE id=%d", $id);
    $hsl = $this->db->query($q);
    $row = $hsl->row_array();
    if ($row) {
      echo json_encode($row);
    } else {
      echo '[]';
    }
    exit();
  }

  public function reloadpenc()
  {
    if (@$_SESSION['akses_modul']['pencipta_data']!="on") {
      redirect('/auth/index', 'refresh');
      exit();
    }

    $q = "SELECT * FROM master_pencipta ORDER BY nama_pencipta ASC";
    $hsl = $this->db->query($q);
    $row = $hsl->result_array();
    if ($row) {
      echo "<table class='table table-bordered' name='vpenc' id='vpenc'>
      <thead>
      <th class='width-sm'>No</th>
      <th>Nama</th>";
      if(@$_SESSION['akses_modul']['pencipta_edit']=='on'){
        echo "<th class='width-sm'></th>";
      }
      if(@$_SESSION['akses_modul']['pencipta_hapus']=='on'){
        echo "<th class='width-sm'></th>";
      }
      echo "</thead>";
      $no = 1;
      foreach ($row as $u) {
        echo "<tr>";
        echo "<td>" . $no . "</td>";
        echo "<td>" . $u['nama_pencipta'] . "</td>";
        if(@$_SESSION['akses_modul']['pencipta_edit']=='on'){
          echo "<td><a data-toggle=\"modal\" data-target=\"#editpenc\" class='edpenc' href='#' id='" . $u['id'] . "' title=\"Edit\"><i class=\"glyphicon glyphicon-edit\"></i> </a></td>";
        }
        if(@$_SESSION['akses_modul']['pencipta_hapus']=='on'){
          echo "<td><a data-toggle=\"modal\" data-target=\"#delpenc\" class='delpenc' href='#' id='" . $u['id'] . "' title=\"Delete\"><i class=\"glyphicon glyphicon-trash\"></i> </a></td>";
        }
        echo "</tr>";
        $no++;
      }
      echo "</table>";
    }
  }
}