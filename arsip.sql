-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2019 at 11:58 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arsip`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_arsip`
--

CREATE TABLE `data_arsip` (
  `id` int(11) NOT NULL,
  `noarsip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pencipta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_pengolah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `uraian` text COLLATE utf8_unicode_ci NOT NULL,
  `ket` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` text COLLATE utf8_unicode_ci,
  `tgl_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_arsip`
--

INSERT INTO `data_arsip` (`id`, `noarsip`, `pencipta`, `unit_pengolah`, `tanggal`, `uraian`, `ket`, `jumlah`, `lokasi`, `file`, `tgl_input`, `tgl_update`, `username`) VALUES
(21, '29/A1/SDM.05/2017', '3', '7', '2017-04-10', 'Surat pemberhentian pegawai oleh Manajer HRD', 'asli', 1, '5', 'SURAT_DINAS_Prosedur_Rekrutmen2.pdf', '2017-11-10 04:30:31', '2019-05-10 09:51:52', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `master_lokasi`
--

CREATE TABLE `master_lokasi` (
  `id` int(11) NOT NULL,
  `nama_lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_lokasi`
--

INSERT INTO `master_lokasi` (`id`, `nama_lokasi`) VALUES
(1, 'Gedung A, Unit II'),
(2, 'Gedung B, Unit III'),
(3, 'Gedung C, Unit IV'),
(4, 'Lokasi'),
(5, 'Gedung C lt 2'),
(6, 'Gedung B lt4');

-- --------------------------------------------------------

--
-- Table structure for table `master_pencipta`
--

CREATE TABLE `master_pencipta` (
  `id` int(11) NOT NULL,
  `nama_pencipta` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_pencipta`
--

INSERT INTO `master_pencipta` (`id`, `nama_pencipta`) VALUES
(5, 'Bidang Hukum dan Tata Laksana'),
(3, 'Bidang Kepegawaian'),
(6, 'Bidang Keuangan'),
(4, 'Bidang Pengadaan'),
(8, 'Bidang Produksi'),
(7, 'Bidang Umum dan Rumah Tangga'),
(9, 'Pencipta'),
(11, 'Bidang QWE');

-- --------------------------------------------------------

--
-- Table structure for table `master_pengolah`
--

CREATE TABLE `master_pengolah` (
  `id` int(11) NOT NULL,
  `nama_pengolah` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_pengolah`
--

INSERT INTO `master_pengolah` (`id`, `nama_pengolah`) VALUES
(6, 'Sekertaris Divisi 3'),
(7, 'Sekertaris Divisi 1'),
(8, 'Sekertaris Divisi 2');

-- --------------------------------------------------------

--
-- Table structure for table `master_user`
--

CREATE TABLE `master_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akses_klas` text COLLATE utf8_unicode_ci NOT NULL,
  `akses_modul` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_user`
--

INSERT INTO `master_user` (`id`, `username`, `password`, `akses_klas`, `akses_modul`) VALUES
(14, 'kepala_seksi', '$2y$10$ddji2PM7WH.uaUPWuujFfe8RcNi1asRE4s3uv.MZhiij1pdQ/I0UC', 'Kepala Seksi', '{\"arsip_data\":\"on\",\"arsip_download\":\"on\",\"arsip_hapus\":\"on\"}'),
(13, 'admin', '$2y$10$HQ/9ZBSXBXefKBDfFHeCsOMMDHTk8VkR0YiCTeymkeaX6mvfe3jM6', 'Admin', '{\"arsip_data\":\"on\",\"arsip_download\":\"on\",\"arsip_tambah\":\"on\",\"arsip_edit\":\"on\",\"arsip_hapus\":\"on\",\"pencipta_data\":\"on\",\"pencipta_tambah\":\"on\",\"pencipta_edit\":\"on\",\"pencipta_hapus\":\"on\",\"pengolah_data\":\"on\",\"pengolah_tambah\":\"on\",\"pengolah_edit\":\"on\",\"pengolah_hapus\":\"on\",\"lokasi_data\":\"on\",\"lokasi_tambah\":\"on\",\"lokasi_edit\":\"on\",\"lokasi_hapus\":\"on\",\"pengguna_data\":\"on\",\"pengguna_tambah\":\"on\",\"pengguna_edit\":\"on\",\"pengguna_hapus\":\"on\"}'),
(15, 'sekertaris', '$2y$10$4jwotV5Yr7SCH2xAnooa4uCFZCnACNLMKUZoyEgHKzUS2i0rhMIdq', 'Sekertaris', '{\"arsip_data\":\"on\",\"arsip_download\":\"on\",\"arsip_tambah\":\"on\",\"arsip_edit\":\"on\"}'),
(16, 'kustom_akses', '$2y$10$RwyYzLIx0oNltEDIvC1kfezJRY6BuVrxlvR8Fs77RXx558sifzD5y', 'Kustom Akses', '{\"arsip_data\":\"on\",\"arsip_download\":\"on\",\"arsip_tambah\":\"on\",\"arsip_hapus\":\"on\",\"pencipta_data\":\"on\",\"pencipta_edit\":\"on\",\"pengolah_data\":\"on\",\"pengolah_tambah\":\"on\",\"pengolah_hapus\":\"on\",\"lokasi_data\":\"on\",\"lokasi_edit\":\"on\",\"lokasi_hapus\":\"on\",\"pengguna_data\":\"on\"}');

-- --------------------------------------------------------

--
-- Table structure for table `user_failed_login`
--

CREATE TABLE `user_failed_login` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `ip` varchar(48) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_arsip`
--
ALTER TABLE `data_arsip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `noarsip` (`noarsip`),
  ADD KEY `pencipta` (`pencipta`),
  ADD KEY `unit_pengolah` (`unit_pengolah`);
ALTER TABLE `data_arsip` ADD FULLTEXT KEY `uraian` (`uraian`);

--
-- Indexes for table `master_lokasi`
--
ALTER TABLE `master_lokasi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_lokasi` (`nama_lokasi`);

--
-- Indexes for table `master_pencipta`
--
ALTER TABLE `master_pencipta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_pencipta` (`nama_pencipta`);

--
-- Indexes for table `master_pengolah`
--
ALTER TABLE `master_pengolah`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_pengolah` (`nama_pengolah`);

--
-- Indexes for table `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_failed_login`
--
ALTER TABLE `user_failed_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_arsip`
--
ALTER TABLE `data_arsip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `master_lokasi`
--
ALTER TABLE `master_lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_pencipta`
--
ALTER TABLE `master_pencipta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `master_pengolah`
--
ALTER TABLE `master_pengolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `master_user`
--
ALTER TABLE `master_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_failed_login`
--
ALTER TABLE `user_failed_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
